const validate = values => {
	const errors = {}
	if (!values.password) {
		errors.password = 'Password is required'
	} else if(values.password.length < 6){
		errors.password = 'Password should be minimum 6 characters long'
	}
	if (!values.passwordconfirm) {
		errors.passwordconfirm = 'Required'
	} else if(values.passwordconfirm != values.password){
		errors.passwordconfirm = 'Password confirmation should match the password.'
	}
	if (!values.email) {
		errors.email = 'Email should be required'
	} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
		errors.email = 'Invalid email address'
	}
	if (!values.sex) {
		errors.sex = 'Gender is required'
	}
	return errors
}

export default validate