import React from 'react'

const renderField = ({ input, label, type, meta: { touched, error } }) => (
	<div className="feild_group">
		<label>{label}</label>
		<div className="feild_cell">
			<input {...input} type={type}/>
			<div className="invalid">
				{touched && error && <span>{error}</span>}
			</div>
		</div>
	</div>
)

export default renderField