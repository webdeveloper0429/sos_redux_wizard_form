import React, { Component } from 'react'
import PropTypes from 'prop-types';
import WizardFormFirstPage from './WizardFormFirstPage'
import WizardFormSecondPage from './WizardFormSecondPage'
import WizardFormThirdPage from './WizardFormThirdPage'
import { Line, Circle } from 'rc-progress';

class WizardForm extends Component {
  constructor(props) {
    super(props)
    this.nextPage = this.nextPage.bind(this)
    this.previousPage = this.previousPage.bind(this)
    this.state = {
      page: 1
    }
  }
  nextPage() {
    this.setState({ page: this.state.page + 1 })
  }

  previousPage() {
    this.setState({ page: this.state.page - 1 })
  }
  onSubmit(){
    alert("asdf");
  }
  render() {
    const { onSubmit } = this.props
    const { page } = this.state
    return (
      <div className="page_home">
        <div className="navigationBar">
          <div className="title">
            {page === 1 && <div>Signup</div>}
            {page === 2 && <div>Signup</div>}
            {page === 3 && <div>Thankyou!</div>}
          </div>
          <div className="progress_div">
            <Line percent={this.state.page*100/3} strokeWidth="1" strokeColor="#4a90e2" strokeLinecap="square"/>
          </div>
        </div>
        <div className="wid_form">
          {page === 1 && <WizardFormFirstPage onSubmit={this.nextPage}/>}
          {page === 2 && <WizardFormSecondPage previousPage={this.previousPage} onSubmit={this.nextPage}/>}
          {page === 3 && <WizardFormThirdPage onSubmit={onSubmit}/>}
        </div>
      </div>
    )
  }
}

// WizardForm.propTypes = {
//   onSubmit: PropTypes.func.isRequired
// }

export default WizardForm