import React from 'react'
import { Field, reduxForm } from 'redux-form'
import validate from './validate'
import renderField from './renderField'

const WizardFormFirstPage = (props) => {
	const { handleSubmit } = props
	return (
		<form onSubmit={handleSubmit}>
			<div className="form_content">
				<Field name="email" type="email" component={renderField} label="Email"/>
				<Field name="password" type="password" component={renderField} label="PASSWORD"/>
				<Field name="passwordconfirm" type="password" component={renderField} label="CONFIRM PASSWORD"/>
			</div>
			<div className="form_footer">
				<button type="submit" className="nextBtn">Next→</button>
			</div>
		</form>
	) 
}

export default reduxForm({
	form: 'wizard',                 // <------ same form name
	destroyOnUnmount: false,        // <------ preserve form data
	forceUnregisterOnUnmount: true,  // <------ unregister fields on unmount
	validate
})(WizardFormFirstPage)