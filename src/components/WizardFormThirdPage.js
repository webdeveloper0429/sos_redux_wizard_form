import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'

class WizardFormThirdPage extends Component {
	constructor(props) {
		super(props);
	}
	customSubmit(data) { 
		const custom = {};
		custom.email = data.email;
		custom.password = data.password;
		custom.date_of_birth = window.birthdata;
		custom.gender = data.sex;
		custom.how_hear_about_us = data.how_hear_about_us;
		const userdata = { "user_data": custom};
		console.log(userdata);
		alert("Please check console log!\n\nAll data is exported in their...\n\nThanks for review this!")
	}
	render() {
		const { handleSubmit } = this.props
		return(
			<form onSubmit={handleSubmit(this.customSubmit)}>
				<div className="form_content">
					<div className="checkImg">
						<img src="check.png" width='200' height='200'/>
					</div>
					<div className="subBtn_div">
						<button type="submit" >Go to Dashboard→</button>
					</div>
				</div>
				
			</form>
		)
	}
}

export default reduxForm({
  form: 'wizard', //Form name is same
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,  // <------ unregister fields on unmount
})(WizardFormThirdPage)