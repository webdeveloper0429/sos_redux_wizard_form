import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import validate from './validate'
import renderField from './renderField'
const renderbirthday = ({ input, label, type, meta: { touched, error } }) => (
	<div className="feild_group">
		<label className="center_label">{label}</label>
		<div className="feild_cell">
			<input  {...input} type="text"  data-format="DD-MM-YYYY" data-template="DD MM YYYY"  value="10-10-2000"/>
			<div className="invalid">
				{touched && error && <span>{error}</span>}
			</div>
		</div>
	</div>
)
const hear = ({ input, label, type, meta: { touched, error } }) => (
	<div className="feild_group">
		<label className="center_label">{label}</label>
		<div className="hear_select">
			<Field name={input.name} component="select">
				<option></option>
				<option value="you">you</option>
				<option value="he">he</option>
				<option value="her">her</option>
			</Field>
		</div>
	 </div>
)
const renderError = ({ meta: { touched, error } }) => (
			<div className="invalid">
				{touched && error && <span>{error}</span>}
			</div>)

class WizardFormSecondPage extends Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		$('input[name=birth]').combodate({
			minYear: 1900,
			maxYear: 2000
		});  
	}
	componentWillUnmount() {
		window.birthdata = $('input[name=birth]').combodate('getValue');
	}
	render() {
		const { handleSubmit, previousPage } = this.props
		return(
			<form onSubmit={handleSubmit}>
				<div className="form_content">
					<Field name="birth" type="date" component={renderbirthday} label="DATE OF BIRTH"/>
					<div className="feild_group">
						<label className="center_label">GENDER</label>
						<div className="gender">
							<Field id="male" name="sex" component="input" type="radio" value="male"/><label htmlFor="male"> MALE</label>
							<Field id="female" name="sex" component="input" type="radio" value="female"/><label htmlFor="female"> FEMALE</label>
							<Field id="unspecified" name="sex" component="input" type="radio" value="unspecified"/><label htmlFor="unspecified"> UNSPECIFIED</label>
							
						</div>
						<Field name="sex" component={renderError}/>
					</div>
					<Field name="how_hear_about_us" component={hear} label="WHERE DID YOU HEAR ABOUT US?"/>
				</div>
				<div className="form_footer">
					<button type="button" className="prevBtn" onClick={previousPage}>Back</button>
					<button type="submit" className="nextBtn">Next→</button>
				</div>
			</form>
		)
	}
}

export default reduxForm({
	form: 'wizard',  //Form name is same
	destroyOnUnmount: false,
	forceUnregisterOnUnmount: true,  // <------ unregister fields on unmount
	validate
})(WizardFormSecondPage)